#!/bin/bash

while [ "$1" != "" ]; do
	case $1 in
		SONARQUBE_CONFIG_TRIES)
			shift
			SONARQUBE_CONFIG_TRIES=$1
			;;
		SONARQUBE_CONFIG_ADMIN_PASSWORD)
			shift
			SONARQUBE_CONFIG_ADMIN_PASSWORD=$1
			;;
		* )
			echo "Unknowed parameter [$1]"
			exit 1
	esac
	shift
done

SONAR_USERNAME=admin
SONAR_PASSWORD=admin
SONAR_HOST=localhost
SONAR_PORT=9000


isSonarRunning() {
	curl -s -u $SONAR_USERNAME:$SONAR_PASSWORD http://$SONAR_HOST:$SONAR_PORT/api/system/health | grep GREEN
	if [ $? -ne 0 ]; then
		return 1
	else
		return 0
	fi
}

if [ -z "$SONARQUBE_CONFIG_TRIES" ]; then
	SONARQUBE_CONFIG_TRIES=60
fi
tries=0
while ! isSonarRunning ; do
	if (( tries > $SONARQUBE_CONFIG_TRIES )); then
		echo "Sonar is not started after $SONARQUBE_CONFIG_TRIES tries..."
		exit 1
	fi
	((tries++))
	sleep 2
done

echo "Sonar is started, let's configure it !"

sonarApi() {
	METHOD=$1
	shift
	URL_PATH=$1
	shift

	curl -s -u $SONAR_USERNAME:$SONAR_PASSWORD -X $METHOD http://$SONAR_HOST:$SONAR_PORT/api$URL_PATH $@

	if [ $? -eq 0 ]; then
		echo "$METHOD $URL_PATH $@"
	else
		echo "warning: request failure for $METHOD $URL_PATH $@"
	fi
}


# Change the admin password

if [ "$SONARQUBE_CONFIG_ADMIN_PASSWORD" ]; then
	sonarApi POST /users/change_password?login=$SONAR_USERNAME\&password=$SONARQUBE_CONFIG_ADMIN_PASSWORD\&previousPassword=$SONAR_PASSWORD
	SONAR_PASSWORD=$SONARQUBE_CONFIG_ADMIN_PASSWORD
fi
